from pycountry import countries

def alpha_3_to_2(alpha_3s: list) -> list:
    alpha_2s = []
    for alpha_3 in alpha_3s:
        current_country = countries.get(alpha_3=alpha_3)
        if current_country:
            alpha_2s.append(current_country.alpha_2)
        else:
            alpha_2s.append(None)
    return alpha_2s
