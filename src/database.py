from typing import Dict
from clickhouse_driver import Client
from csv import DictReader
from pathlib import Path


class DbClient():
    TABLE_NAME = 'Countries'

    def __init__(self) -> None:
        self.client = Client('localhost')
        self.setup()
    
    def get_countries(self) -> list:
        return self.client.execute(
            f'SELECT * FROM {self.TABLE_NAME}')

    def setup(self) -> None:
        if self.TABLE_NAME not in self._current_tables():
            self._create_table()
            countries = self._load_countries_from_file()
            self._insert_countries(countries)

    def _current_tables(self) -> list:
        tubled_tables = self.client.execute('SHOW TABLES')
        tables = [table[0] for table in tubled_tables]
        return tables

    def _create_table(self) -> None:
        self.client.execute(f'''CREATE TABLE {self.TABLE_NAME} (
            id UInt16,
            country String,
            alpha_3 String
            ) 
            ENGINE = Memory
            ''')

    def _load_countries_from_file(self) -> dict:
        filepath = Path.cwd() / 'data' / 'countries.csv'
        countries = dict()
        with open(filepath) as file:
            reader = DictReader(file)
            for row in reader:
                countries[row["Alpha-3"]] = row["Country"]
        return countries

    def _insert_countries(self, countries: dict) -> None:
        id = (i for i in range(1,1000))
        self.client.execute(
            f'''INSERT INTO {self.TABLE_NAME} (id, country, alpha_3) VALUES''',
            ((next(id), country, alpha_3) for alpha_3, country in countries.items())
            )
