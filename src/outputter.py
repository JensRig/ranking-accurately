from prettytable import PrettyTable
from pandas import DataFrame

class Outputter():
    COLUMNS = ["id", "country", "alpha_2"]

    def __init__(self) -> None:
        self.pt = PrettyTable()

    def to_terminal(self, df: DataFrame) -> None:
        for column in self.COLUMNS:
            self.pt.add_column(column, df[column].to_list())
        print(self.pt)