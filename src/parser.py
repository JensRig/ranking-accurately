import argparse


class ArgumentParser():
    ORDERS = ['asc', 'desc']
    HELP = 'Type asc for ascending or desc for descending'

    def __init__(self) -> None:
        self.parser = argparse.ArgumentParser(
            description='Get a sorted list of countries in Alpha-2 format.')
        self.parser.add_argument(
            '--sort-order',
            dest='order',
            help=f"{self.HELP}.")

    def parse_arguments(self):
        try:
            args = self.parser.parse_args()
            assert args.order in self.ORDERS
        except Exception:
            args = self.handle_missing_argument()
        return args.order

    def handle_missing_argument(self):
        while True:
            order = input(f"{self.HELP}: ")
            if order in self.ORDERS:
                break
            else:
                print("Input not recognized.")
        return self.parser.parse_args(['--sort-order', order])
