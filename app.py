from pandas import DataFrame
from src.parser import ArgumentParser
from src.database import DbClient
from src.converter import alpha_3_to_2
from src.outputter import Outputter


class Runner():
    COLUMNS = ["id", "country", "alpha_3"]

    def __init__(self) -> None:
        self.parser = ArgumentParser()
        self.db = DbClient()
        self.output = Outputter()

    def run(self):
        order = self.parser.parse_arguments()
        country_query_result = self.db.get_countries()
        self.load_to_dataframe(country_query_result, order)
        self.convert_to_alpha_2()
        self.output.to_terminal(self.df)

    def load_to_dataframe(self, query_result: list, order: str) -> DataFrame:
        self.df = DataFrame(query_result, columns=self.COLUMNS)
        if order == "desc":
            df = self.df.iloc[::-1]
            self.df = df           

    def convert_to_alpha_2(self):
        self.df['alpha_2'] = alpha_3_to_2(self.df['alpha_3'].to_list())
        self.df.dropna()


if __name__ == "__main__":
    app = Runner()
    app.run()
